%
% ASP program for the jobs puzzle.
%
% Variation 4_2: injection of inconsistency (Robin is a female).
%
% Added four background rule:
%    1. A person who is a male is a husband of exactly one other person, or that person is null.
%    2. A person who is a female has exactly one husband or that husband is null.
%    3. Exclude that person X is a husband of Y and person Z is a husband of X simultaneously.
%    4. If it is not derivable that person X is person Y’s husband, then X is not Y’s husband.
%
% This program is ran on Clingo and Asprin extension (http://www.cs.uni-potsdam.de/asprin/).
% 
% How to run the program (Linux/Mac Environment):
% ./asprin /path/to/file  asprin.lib 0 --show-optimal
%
%

%domain of constants.
dom(roberta;thelma;robin;pete;chef;guard;nurse;operator;police;teacher;actor;boxer).

%domain of predicate terms.
hu(person(X)):-dom(X).
hu(job(X)):-dom(X).
hu(hold(X,Y)):-dom(X),dom(Y).
hu(male(X)):-dom(X).
hu(female(X)):-dom(X).
hu(husband(X,Y)):-dom(X),dom(Y).
hu(educated(X)):-dom(X).

% Roberta is a person. Thelma is a person. Robin is a person. Pete is a person.
truth(person(roberta),t).
truth(person(thelma),t).
truth(person(robin),t).
truth(person(pete),t).

% Roberta is female. Thelma is female.
truth(female(roberta),t).
truth(female(thelma),t).

% Robin is male. Pete is male.
truth(male(robin),t). 
truth(male(pete),t).

% Exclude that a person is male and that the person is female.
truth(male(X),t);truth(female(X),t):-truth(person(X),t).
truth(male(X),f);truth(female(X),f):-truth(person(X),t).
truth(female(X),top):-truth(person(X),t),truth(male(X),top).
truth(male(X),top):-truth(person(X),t),truth(female(X),top).

% If there is a job then exactly one person truth the job. 
1 {truth(hold(X,Y),t):truth(person(X),t)} 1:-truth(job(Y),t). % 1.1
truth(hold(X,Y),f):-truth(job(Y),t),truth(person(X),t),not truth(hold(X,Y),t). %1.2

% If there is a person then the person truth exactly two jobs.
2 {truth(hold(X,Y),t):truth(job(Y),t)} 2:-truth(person(X),t). % 2.1
% truth(hold(X,Y),f):-truth(person(X),t),truth(job(Y),t),not truth(hold(X,Y),t). % 2.2 same as 1.2 

% Chef is a job. Guard is a job. Nurse is a job. Operator is a job. Police is a job. Teacher is a job. Actor is a job. Boxer is a job. 
truth(job(chef),t).
truth(job(guard),t). 
truth(job(nurse),t). 
truth(job(operator),t). 
truth(job(police),t). 
truth(job(teacher),t). 
truth(job(actor),t). 
truth(job(boxer),t). 

% If a person truth a job as nurse then the person is male.
truth(male(X),t);truth(hold(X,nurse),f):-truth(person(X),t),truth(job(nurse),t),not truth(person(X),top),not truth(job(nurse),top).

% If a person truth a job as actor then the person is male.
truth(male(X),t);truth(hold(X,actor),f):-truth(person(X),t),truth(job(actor),t),not truth(person(X),top),not truth(job(actor),top).

% If a first person truth a job as chef and a second person truth a job as telephone operator then the second person is a husband of the first person.
truth(husband(Y,X),t);truth(hold(X,chef),f);truth(hold(Y,operator),f):-truth(person(X),t),truth(job(chef),t),truth(person(Y),t),truth(job(operator),t),not truth(person(X),top),not truth(job(chef),top),not truth(person(Y),top),not truth(job(operator),top).

% If a first person is a husband of a second person then the first person is male.
truth(male(X),t);truth(husband(X,Y),f):-truth(person(X),t),truth(person(Y),t),not truth(person(X),top),not truth(person(Y),top).

% If a first person is a husband of a second person then the second person is female.
truth(female(Y),t);truth(husband(X,Y),f):-truth(person(X),t),truth(person(Y),t),not truth(person(X),top),not truth(person(Y),top).

% Exclude that Roberta truth a job as boxer.
truth(hold(roberta,boxer),f):-truth(job(boxer),t),not truth(job(boxer),top).

% Exclude that Pete is educated.
truth(educated(pete),f).

% If a person truth a job as nurse then the person is educated.
truth(educated(X),t);truth(hold(X,nurse),f):-truth(person(X),t),truth(job(nurse),t), not truth(person(X),top),not truth(job(nurse),top).

% If a person truth a job as police officer then the person is educated.
truth(educated(X),t);truth(hold(X,police),f):-truth(person(X),t),truth(job(police),t),not truth(person(X),top),not truth(job(police),top).

% If a person truth a job as teacher then the person is educated.
truth(educated(X),t);truth(hold(X,teacher),f):-truth(person(X),t),truth(job(teacher),t),not truth(person(X),top),not truth(job(teacher),top).

% Exclude that Roberta truth a job as chef.
truth(hold(roberta,chef),f):-truth(job(chef),t),not truth(job(chef),top).

% Exclude that Roberta truth a job as police officer.
truth(hold(roberta,police),f):-truth(job(police),t),not truth(job(police),top).

% Exclude that a person truth a job as chef and that the person truth a job as police officer.
truth(hold(X,chef),f);truth(hold(X,police),f):-truth(person(X),t),truth(job(chef),t),truth(job(police),t),not truth(person(X),top),not truth(job(chef),top),not truth(job(police),top).

% Robin is a female. — Variation 4_2 (modification of Variation 2)
truth(female(robin),t).

% Background knowledge of husband relation 

% A person who is a male is a husband of exactly one other person, or that person is null.
1 {truth(husband(X,Y),t):truth(person(Y),t);truth(husband(X,null),t)} 1:-truth(person(X),t),truth(male(X),t),not truth(male(X),top).

% A person who is a female has exactly one husband or that husband is null.
truth(husband(X,Y),f):-truth(person(X),t),truth(person(Y),t),not truth(husband(X,Y),t).

% Exclude that person X is a husband of Y and person Z is a husband of X simultaneously.
1 {truth(husband(X,Y),t):truth(person(X),t);truth(husband(null,Y),t)} 1:-truth(person(Y),t),truth(female(Y),t),not truth(female(Y),top).

% If it is not derivable that person X is person Y’s husband, then X is not Y’s husband. 
:- truth(husband(X,_),t), truth(husband(_,X),t), X != null.

% Background axioms
truth(X,top) :- truth(X,t), truth(X,f).
truth(X,t) :- truth(X,top).
truth(X,f) :- truth(X,top).
truth(X,bottom) :- hu(X).

% Preference relations
#preference(all,lexico){
	4::name(p4);
	3::name(p3);
	2::name(p2);
	1::name(p1)
}.
#preference(p4,subset){ truth(person(X),top) : dom(X);truth(job(Y),top) : dom(Y) }. % person and job information (highest degree of confidence)
#preference(p3,subset){ truth(male(pete),top);truth(female(thelma),top);truth(female(roberta),top)}. % gender information for Pete, Thelma, and Roberta (second highest degree of confidence)
#preference(p2,subset){ truth(hold(X,Y),top):dom(X),dom(Y)}. % hold job information (third highest degree of confidence)
#preference(p1,subset){ truth(X,top) : hu(X) }. % preference relation to find most e-consistent models
#optimize(all).

% Projection of truth/2 predicates onto who holds which jobs.
o(hold(X,Y),t):-truth(hold(X,Y),t).

% Projection of truth/2 predicates onto husband information.
o(husband(X,Y),t):-truth(husband(X,Y),t).

% truth/2 predicates are projected onto inconsistent information (top-predicates in APC_LP).
top(X):-truth(X,top).

#show o/2.
#show top/1.
