dom(dominique;naren;ignace;olivier;philippe;pascal;
  1;2;3;4;5;6).

hu(runner(R)) :- dom(R).
hu(position(R)) :- dom(R).
hu(has_position(R,P)) :- dom(R),dom(P).
hu(before(R1,R2)) :- dom(R1),dom(R2).

% Dominique, Ignace, Naren, Olivier, Philippe, and Pascal have arrived as the first six at the Paris marathon. 
truth(runner(dominique),t).
truth(runner(naren),t).
truth(runner(ignace),t).
truth(runner(olivier),t).
truth(runner(philippe),t).
truth(runner(pascal),t).

% We consider the first six positions.
truth(position(1),t).
truth(position(2),t).
truth(position(3),t).
truth(position(4),t).
truth(position(5),t).
truth(position(6),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Every runner arrives in exactly one position.
1 { truth(has_position(R,P),t) : truth(position(P),t) } 1 :- truth(runner(R),t).

% Every position belongs to exactly one runner.
1 { truth(has_position(R,P),t) : truth(runner(R),t) } 1 :- truth(position(P),t).

% Complete knowledge of runner positions:
truth(has_position(R,P),f) :- 
  truth(runner(R),t),
  truth(position(P),t),
  not truth(has_position(R,P),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Olivier has not arrived last.
truth(has_position(olivier,6),f).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If a runner R1 is allocated to a position P1 and another runner R2 is allocated to a position P2 and P1 is smaller than P2 then R1 is 'before' R2.
truth(before(R1,R2),t) :- truth(runner(R1),t), 
  truth(runner(R2),t),
  truth(has_position(R1,P1),t),
  truth(position(P1),t),
  truth(has_position(R2,P2),t),
  truth(position(P2),t),
  P1 < P2,
  not truth(has_position(R1,P1),top),
  not truth(has_position(R2,P2),top).

truth(before(R2,R1),f) :- truth(runner(R1),t),
  truth(runner(R2),t),
  truth(has_position(R1,P1),t),
  truth(position(P1),t),
  truth(has_position(R2,P2),t),
  truth(position(P2),t),
  P1 < P2,
  not truth(has_position(R1,P1),top),
  not truth(has_position(R2,P2),top).

% Complete knowledge for before:
truth(before(R1,R2),f) :- 
  truth(runner(R1),t), 
  truth(runner(R2),t),
  not truth(before(R1,R2),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dominique, Pascal and Ignace have arrived before Naren and Olivier.
truth(before(dominique,naren),t).
truth(before(pascal,naren),t).
truth(before(ignace,naren),t).
truth(before(dominique,olivier),t).
truth(before(pascal,olivier),t).
truth(before(ignace,olivier),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dominique who was third last year has improved this year.
truth(has_position(dominique,1),t) ; truth(has_position(dominique,2),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Philippe is among the first four.
truth(has_position(philippe,1),t) ; truth(has_position(philippe,2),t) ; truth(has_position(philippe,3),t) ; truth(has_position(philippe,4),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Igance has arrived neither in second nor third position.
truth(has_position(ignace,2),f).
truth(has_position(ignace,3),f).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pascal has beaten Naren by three positions.
truth(has_position(naren,P2),t) :- 
  truth(has_position(pascal,P1),t),
  truth(position(P1),t),
  truth(position(P2),t),
  P1 + 3 = P2,
  not truth(position(P1),top),
  not truth(position(P2),top),
  not truth(has_position(pascal,P1),top).

truth(has_position(pascal,P1),t) :- 
  truth(has_position(naren,P2),t),
  truth(position(P2),t),
  truth(position(P1),t),
  P1 + 3 = P2,
  not truth(position(P1),top),
  not truth(position(P2),top),
  not truth(has_position(naren,P2),top).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Neither Ignace nor Dominique are on the fourth position.
truth(has_position(ignace,4),f).
truth(has_position(dominique,4),f).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% APC_ASP
truth(X,top) :- truth(X,t), truth(X,f).
truth(X,t) :- truth(X,top).
truth(X,f) :- truth(X,top).
%truth(X,bottom) :- hu(X).

% Preference relations
#preference(all,lexico){
  4::name(p4);
  3::name(p3);
%  2::name(p2);
  1::name(p1)
}.

#preference(p4,subset){ 
  truth(runner(R),top):dom(R);
  truth(position(P),top):dom(P)
}.
#preference(p2,subset){
  truth(before(R1,R2),top):dom(R1),dom(R2)
}.
#preference(p3,subset){
  truth(has_position(R,P),top):dom(R),dom(P);
  truth(before(R1,R2),top):dom(R1),dom(R2)
}.
#preference(p1,subset){ truth(X,top) : hu(X) }.
#optimize(all).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% print stuff

has_position(R,P) :-
  truth(has_position(R,P),t).
  
#show has_position/2.

top(X) :-
  truth(X,top).
#show top/1.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Philippe arrives before Dominique.
truth(before(philippe,dominique),t).
