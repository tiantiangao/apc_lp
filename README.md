# README #
### What is this repository for? ###

This repository contains the ASP programs for the jobs puzzle with inconsistency.

### Set up ###

Programs are executed on Clingo and Asprin Extension.

http://www.cs.uni-potsdam.de/asprin/

### How-to run the program ###

Linux/Mac: ./asprin /path/to/file  asprin.lib 0 --show-optimal