%
% ASP program for the jobs puzzle.
%
% Variation 3: injection of inconsistency (Milk is not drunk in the middle house).
%
% This program is ran on Clingo and Asprin extension (http://www.cs.uni-potsdam.de/asprin/).
% 
% How to run the program (Linux/Mac Environment):
% ./asprin /path/to/file  asprin.lib 0 --show-optimal
%
%

dom(1;2;3;4;5;
	yellow;blue;red;ivory;green;
	norwegian;ukrainian;englishman;spaniard;japanese;
	water;tea;milk;orange;coffee;
	kools;chesterfield;oldgold;luckystrike;parliament;
	fox;horse;snails;dog;zebra).

hu(house(X)) :-dom(X).

hu(color(X)) :-dom(X).
hu(nationality(X)) :-dom(X).
hu(drink(X)) :-dom(X).
hu(smoke(X)) :-dom(X).
hu(pet(X)) :-dom(X).

hu(house_color(X,Y)) :-dom(X),dom(Y).
hu(house_nationality(X,Y)) :-dom(X),dom(Y).
hu(house_drink(X,Y)) :-dom(X),dom(Y).
hu(house_smoke(X,Y)) :-dom(X),dom(Y).
hu(house_pet(X,Y)) :-dom(X),dom(Y).

hu(next(X,Y)) :-dom(X),dom(Y).
hu(right(X,Y)) :-dom(X),dom(Y).

% 1. There are five houses.
truth(house(1),t).
truth(house(2),t).
truth(house(3),t).
truth(house(4),t).
truth(house(5),t).

% A house is next to another house if their house numbers differ by 1 
truth(next(X,Y),t) :-
	truth(house(X),t),
	truth(house(Y),t),
	|X - Y| = 1,
	not truth(house(X),top),
	not truth(house(Y),top).
	
% A house is not next to another house if their house numbers do not differ by 1
truth(next(X,Y),f) :-
	truth(house(X),t),
	truth(house(Y),t),
	|X - Y| != 1,
	not truth(house(X),top),
	not truth(house(Y),top).

% A house numbered X is to the right of another house numbered Y if X - 1 = Y.
truth(right(X,Y),t) :-
	truth(house(X),t),
	truth(house(Y),t),
	X - 1 = Y,
	not truth(house(X),top),
	not truth(house(Y),top).
	
% A house numbered X is not to right of another house numbered Y if X - 1 != Y. 
truth(right(X,Y),f) :-
	truth(house(X),t),
	truth(house(Y),t),
	X - 1 != Y,
	not truth(house(X),top),
	not truth(house(Y),top).

% There are five colors: yellow, blue, red, ivory, green.
truth(color(yellow),t).
truth(color(blue),t).
truth(color(red),t).
truth(color(ivory),t).
truth(color(green),t).

% There are five nationalities: norwegian, ukranian, englishman, spaniard, japanese.
truth(nationality(norwegian),t).
truth(nationality(ukrainian),t).
truth(nationality(englishman),t).
truth(nationality(spaniard),t).
truth(nationality(japanese),t).

% There are five drinks: water, tea, milk, orange, coffee.
truth(drink(water),t).
truth(drink(tea),t).
truth(drink(milk),t).
truth(drink(orange),t).
truth(drink(coffee),t).

% There are five smokes: kools, chesterfield, oldgold, luckstrike, parliament.
truth(smoke(kools),t).
truth(smoke(chesterfield),t).
truth(smoke(oldgold),t).
truth(smoke(luckystrike),t).
truth(smoke(parliament),t).

% There are five pets: fox, horse, snails, dog, zebra.
truth(pet(fox),t).
truth(pet(horse),t).
truth(pet(snails),t).
truth(pet(dog),t).
truth(pet(zebra),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Every house has exactly one color.
1{truth(house_color(H,C),t) : truth(color(C),t)}1 :- 
	truth(house(H),t).

% Every color is for exactly one house.
1{truth(house_color(H,C),t) : truth(house(H),t)}1 :- 
	truth(color(C),t).

% Complete knowledge of house colors
truth(house_color(H,C),f) :-
	truth(house(H),t),
	truth(color(C),t),
	not truth(house_color(H,C),t).
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Every house has exactly one nationality.
1{truth(house_nationality(H,N),t) : truth(nationality(N),t)}1 :- 
	truth(house(H),t).

% Every nationality is for exactly one house.
1{truth(house_nationality(H,N),t) : truth(house(H),t)}1 :- 
	truth(nationality(N),t).

% Complete knowledge of house nationalities
truth(house_nationality(H,N),f) :-
	truth(house(H),t),
	truth(nationality(N),t),
	not truth(house_nationality(H,N),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Every house has exactly one drink.
1{truth(house_drink(H,D),t) : truth(drink(D),t)}1 :- 
	truth(house(H),t).

% Every drink is for exactly one house.
1{truth(house_drink(H,D),t) : truth(house(H),t)}1 :- 
	truth(drink(D),t).

% Complete knowledge of house drinks
truth(house_drink(H,D),f) :-
	truth(house(H),t),
	truth(drink(D),t),
	not truth(house_drink(H,D),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Every house has exactly one smoke.
1{truth(house_smoke(H,S),t) : truth(smoke(S),t)}1 :- 
	truth(house(H),t).

% Every smoke is for exactly one house.
1{truth(house_smoke(H,S),t) : truth(house(H),t)}1 :- 
	truth(smoke(S),t).

% Complete knowledge of house smokes
truth(house_smoke(H,S),f) :-
	truth(house(H),t),
	truth(smoke(S),t),
	not truth(house_smoke(H,S),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Every house has exactly one pet.
1{truth(house_pet(H,P),t) : truth(pet(P),t)}1 :- 
	truth(house(H),t).

% Every pet is for exactly one house.
1{truth(house_pet(H,P),t) : truth(house(H),t)}1 :- 
	truth(pet(P),t).

% Complete knowledge of house pets
truth(house_pet(H,P),f) :-
	truth(house(H),t),
	truth(pet(P),t),
	not truth(house_pet(H,P),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2. The Englishman house_nationality in the red house.
% If a house's color is red then Englishman house_nationality in the house.
truth(house_color(H,red),t) ; truth(house_nationality(H,englishman),f):-
	truth(house(H),t),
	not truth(house(H),top).
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3. The Spaniard owns the dog.
% In the with the Spaniard the pet is the dog.
truth(house_pet(H,dog),t) ; truth(house_nationality(H,spaniard),f):-
	truth(house(H),t),
	not truth(house(H),top).
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4. Coffee is drunk in the green house.
truth(house_drink(H,coffee),t) ; truth(house_color(H,green),f):-
	truth(house(H),t),
	not truth(house(H),top).
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5. The Ukrainian drinks tea.
% The house with the Ukrainian drinks tea.
truth(house_drink(H,tea),t);truth(house_nationality(H,ukrainian),f) :-
	truth(house(H),t),
	not truth(house(H),top).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6. The green house is immediately to the right of the ivory house.

truth(house_color(1,green),f).
truth(house_color(5,ivory),f).

truth(house_color(Left,ivory),t) ; truth(house_color(Right,green),f):-
	truth(house(Left),t),
	truth(house(Right),t),
	truth(right(Right,Left),t),
	not truth(house(Left),top),
	not truth(house(Right),top),
	not truth(right(Right,Left),top).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7. The Old Gold smoker owns snails.
truth(house_pet(H,snails),t) ; truth(house_smoke(H,oldgold),f):-
	truth(house(H),t),
	not truth(house(H),top).
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8. Kools are smoked in the yellow house.
truth(house_smoke(H,kools),t) ; truth(house_color(H,yellow),f):-
	truth(house(H),t),
	not truth(house(H),top).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9. Milk is drunk in the middle house.
truth(house_drink(3,milk),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 10. The Norwegian lives in the first house.
truth(house_nationality(1,norwegian),t).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 11. The man who smokes chesterfield lives in the house next to the man with the fox.
truth(house_pet(H1,fox),t) ; truth(house_pet(H3,fox),t) ; truth(house_smoke(H2,chesterfield),f):- 
	truth(house(H1),t),
	truth(next(H1,H2),t),
	truth(house(H2),t),
	truth(next(H2,H3),t),
	truth(house(H3),t),
	H1 != H3,
	not truth(house(H1),top),
	not truth(next(H1,H2),top),
	not truth(house(H2),top),
	not truth(next(H2,H3),top),
	not truth(house(H3),top).

truth(house_pet(2,fox),t) ; truth(house_smoke(1,chesterfield),f).

truth(house_pet(4,fox),t) ; truth(house_smoke(5,chesterfield),f).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12. Kools are smoked in the house next to the house where the horse is kept.
truth(house_pet(H1,horse),t) ; truth(house_pet(H3,horse),t) ; truth(house_smoke(H2,kools),f) :- 
	truth(house(H1),t),
	truth(house(H2),t),
	truth(house(H3),t),
	H1 != H3,
	truth(next(H1,H2),t),
	truth(next(H2,H3),t),
	not truth(house(H1),top),
	not truth(house(H2),top),
	not truth(house(H3),top),
	not truth(next(H1,H2),top),
	not truth(next(H2,H3),top).
	
truth(house_pet(2,horse),t) ; truth(house_smoke(1,kools),f).

truth(house_pet(4,horse),t) ; truth(house_smoke(5,kools),f).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 13. The Lucky Strike smoker drinks orange juice.
truth(house_drink(H,orange),t) ; truth(house_smoke(H,luckystrike),f) :-
	truth(house(H),t),
	not truth(house(H),top).
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 14. The Japanese smokes parliament.
truth(house_smoke(H,parliament),t) ; truth(house_nationality(H,japanese),f):-
	truth(house(H),t),
	not truth(house(H),top).
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 15. The Norwegian lives next to the blue house.
truth(house_color(H1,blue),t) ; truth(house_color(H3,blue),t) ; truth(house_nationality(H2,norwegian),f):- 
	truth(house(H1),t),
	truth(house(H2),t),
	truth(house(H3),t),
	H1 != H3,
	truth(next(H1,H2),t),
	truth(next(H2,H3),t),
	not truth(house(H1),top),
	not truth(house(H2),top),
	not truth(house(H3),top),
	not truth(next(H1,H2),top),
	not truth(next(H2,H3),top).

truth(house_color(2,blue),t) ; truth(house_nationality(1,norwegian),f).

truth(house_color(4,blue),t) ; truth(house_nationality(5,norwegian),f).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% APC_ASP
truth(X,top) :- truth(X,t), truth(X,f).
truth(X,t) :- truth(X,top).
truth(X,f) :- truth(X,top).
%truth(X,bottom) :- hu(X).

% Preference relations
#preference(all,lexico){
	3::name(p3);
	2::name(p2);
	1::name(p1)
}.

#preference(p3,subset){ 
	truth(house(X1),top):dom(X1);
	truth(next(X2,Y2),top):dom(X2),dom(Y2);
	truth(right(X3,Y3),top):dom(X3),dom(Y3);
	truth(color(X4),top):dom(X4);
	truth(nationality(X5),top):dom(X5);
	truth(drink(X6),top):dom(X6);
	truth(smoke(X7),top):dom(X7);
	truth(pet(X8),top):dom(X8)
}.
#preference(p2,subset){ 
	truth(house_color(X1,Y1),top):dom(X1),dom(Y1);
	truth(house_nationality(X2,Y2),top):dom(X2),dom(Y2);
	truth(house_drink(X3,Y3),top):dom(X3),dom(Y3);
	truth(house_smoke(X4,Y4),top):dom(X4),dom(Y4);
	truth(house_pet(X5,Y5),top):dom(X5),dom(Y5)
}.
#preference(p1,subset){ truth(X,top) : hu(X) }.
#optimize(all).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% print stuff

tuple(H,C,N,D,S,P) :-
	truth(house_color(H,C),t),
	truth(house_nationality(H,N),t),
	truth(house_drink(H,D),t),
	truth(house_smoke(H,S),t),
	truth(house_pet(H,P),t).
#show tuple/6.

top(X) :-
	truth(X,top).
#show top/1.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inconsistencies
% Milk is not drunk in the middle house. -- Variation 3
truth(house_drink(3,milk),f).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
